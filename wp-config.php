<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'brew');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'nPVqzMStMMsfdnz@kLmquSC:IZN7Uxz}3*ZPn{U2>zkehk@f-7k:s5TBLH[#/[5?');
define('SECURE_AUTH_KEY',  'T[Nw>ExIVR7|#5*TYDo cfIS}|@UT!mW}3=*`VQ#!3[>+T;ivS:q`aRl.T`-q &O');
define('LOGGED_IN_KEY',    'RInk0]HTkhj4&cdF>%J/$R*6_?F<pr?b,,kb/+H8lrE(+.DIxV&-Zny{ti:Q/7:D');
define('NONCE_KEY',        'T4u88L&Rc=zmB#XxK:8m>hiWLF?vmK]K?(NIu!6?dF5ICl).]9)-God}[XDiv@Ze');
define('AUTH_SALT',        'damS10Z?Xvx}|#Arx@t0<!BkaS8f=cLti^k:_{B1P%5fDZWWt6WSazg7oI!py.s{');
define('SECURE_AUTH_SALT', '.Z}Jm9m*4L=E57}dy;#gZ~)Nw-E1.~Z51X.n+clq(T@*ZHC=awP<xF@k9)]Gav7S');
define('LOGGED_IN_SALT',   '<5F)JK6F37I)`oBW_4a? ;|iJYaEfJCv=zT]x[RF`4|@Lu+&8_!u}{Fq[Af3WZ]n');
define('NONCE_SALT',       'Ohm9p%|Hm+MCLkO$pr^X}#`M5QY[%zs~tYfVfwz?r]h9j%e%B@c O3jwjRkW8I1H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
