<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php wp_head(); ?>
</head>
<body>
  <div id="backTop">
    <img src="<?php echo get_stylesheet_directory_uri(). '/images/top.png'; ?>" />
  </div>
  <div class="header-wrapper">
    <div class="menu-icon">
      <span></span>
      <span></span>
      <span></span>
    </div>
    <div class="logo-wrapper">
      <?php
      if(is_front_page()) {
       
      } else {
        if ( function_exists( 'the_custom_logo' ) ) {
          the_custom_logo();
        }
      }?>
      </div>

    <div class="top-social pc-only">
      <?php if ( has_nav_menu( 'social' ) ) : 
        wp_nav_menu( array(
          'theme_location' => 'social',
          'menu_class'     => 'nav-menu'
        ) );
      endif; ?>
    </div>
    <div class="sp-only search-icon">
        <ul>
            <li data-target="#searchModal" data-toggle="modal"><a><span class="fa fa-search"></span></a></li>
        </ul>
    </div>
  </div>
  <div class="search-box modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-body">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
                  <?php get_search_form(); ?>
              </div>
          </div>
      </div>
  </div>
  <div id="myNav" class="overlay">
  <div class="logo-wrapper">
      <?php
             
              if(is_front_page()) {
                if ( function_exists( 'the_custom_logo' ) ) {
                  the_custom_logo();
                }
              } else {
                
              }?>
    </div>
      <!-- Overlay content -->
      <div class="overlay-content" style="<?php echo (is_front_page())? 'top: 0px;': 'top: 15%;'; ?> ">
        <?php if ( has_nav_menu( 'primary' ) ) : 
        wp_nav_menu( array(
          'theme_location' => 'primary',
          'menu_class'     => 'nav-menu',
        ) );
      endif; ?>
         <ul class="menu-social">
          <li class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon1.gif'; ?>" alt=""></li>
          <li class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon2.gif'; ?>" alt=""></li>
          <li class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon3.gif'; ?>" alt=""></li>
         </ul>
      </div>
</div>
<script type="text/javascript">
var url = "<?php echo site_url() ?>";
</script>