<?php
/**
 * Template for displaying search forms in Twenty Sixteen
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>




       <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                      <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s">
                          <button type="submit"><span class="fa fa-search"></span></button>
                      </div>
            </form>
