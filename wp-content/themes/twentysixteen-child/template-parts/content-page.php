<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
global $post;

 ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="intro-content">
		<h2><?php the_title(); ?></h2>
	</div>
	<?php
	$terms = get_terms( array(
			'taxonomy' => $post->post_name,
			'hide_empty' => false,
		) );
		?>
		<div class="container">
			<div class="row">
		<?php
		foreach( $terms as $cat ) {?>
			<div class="col-md-3 col-sm-4 col-xs-6 search-item">
				<a class="search-img" href="<?php echo site_url() .'/'. $post->post_name .'/'. $cat->slug ?> ">
					<img src="<?php echo ( z_taxonomy_image_url($cat->term_id) ) ? z_taxonomy_image_url($cat->term_id) : get_stylesheet_directory_uri(). '/images/placeholder.gif'; ?>">
					</a>
					<div class="subpage-intro search">
					<h2><a href="<?php echo site_url() .'/'. $post->post_name .'/'. $cat->slug ?> " rel="bookmark"><?php echo $cat->name; ?></a></h2>
					</div>
					&nbsp;
					
				</div>
		<?php 
			} ?>
			</div>
		</div>
		
	
</article><!-- #post-## -->
<script>
	var vidUrl_id = '';
</script>
