<?php
/**
 * The template part for displaying results in search pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
global $post;
?>
<div class="col-md-3 col-sm-4 col-xs-6 search-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php
		$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large'); 
		if($image == null) {
			$image_src = site_url() . '/wp-content/uploads/2018/05/placeholder.gif';
		} else {
			$image_src = $image[0];
		}
		 ?>
		<a href="<?php the_permalink(); ?>" class="search-img"><img src="<?php echo $image_src; ?>" alt=""></a>
		<div class="subpage-intro search">
			<h2><a href="<?php echo esc_url( get_permalink() )?>" rel="bookmark"><?php echo the_title() ?></a></h2>
		</div>
		
	</article>
</div>
<style>
.pagination .nav-links:before,
.pagination .nav-links:after {
	display: none;
}
.pagination .nav-links {
	padding-right: 0;
}
</style>
<?php
