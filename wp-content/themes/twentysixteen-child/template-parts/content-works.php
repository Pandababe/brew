<main id="main" class="" role="main">
<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="intro-content">
							<h2>WHAT WE DO</h2>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="filters">

							<div class="custom-drop-wrap">
								<div id="drop_sort" class="drop-down__button">
									<span class="drop-text">RECENT WORKS</span>
									<!-- <span class="drop-text">all</span> -->
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content">
									<ul class="button-group" data-sort="time">
										<li data-filter="DESC" class="active">Recent works</li>
										<li data-filter="ASC">Archive</li>
									</ul>
								</div>
							</div>
							<div class="custom-drop-wrap">
								<div id="drop_director" class="drop-down__button">
									<span class="drop-text">DIRECTORS</span>
									<!-- <span class="drop-text">all</span> -->
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content">
									<ul data-filter-group="director" class="button-group">
										<?php echo cat_terms('director') ?>
									</ul>
								</div>
							</div>
							<div class="custom-drop-wrap">
								<div id="drop_category" class="drop-down__button">
									
									<span class="drop-text">OUR WORK</span>
									<!-- <span class="drop-text">all</span> -->
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content" id="cat-works">
									<ul data-filter-group="category" class="button-group">
										<?php echo cat_terms('works') ?>
									</ul>
								</div>
							</div>


						</div>
						
						<div class="services-wrapper works-page">
							<div class="services-grid">
								<div class="lds-blocks">
								<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="100px" height="100px" viewBox="0 0 261.961 235.781" enable-background="new 0 0 261.961 235.781"
	 xml:space="preserve">
<path fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" d="M69.5,135.657c0,0-8,43.5,32.5,61.5"/>
<path fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" d="M239.492,128.511"/>
<path fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" d="M227.393,135.657c0,0,8,43.5-32.5,61.5"/>
<path fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" d="M45,197.157h208c0,0-22.215,36-54.107,33.5H97.5
	C61,230.657,45,197.157,45,197.157h2.5"/>
<line fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" x1="69.5" y1="135.657" x2="227.393" y2="135.657"/>
<line fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" x1="69.5" y1="135.657" x2="64.5" y2="135.657"/>
<line fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" x1="227.393" y1="135.657" x2="231.5" y2="135.657"/>
<ellipse fill="none" stroke="#808080" stroke-width="10" stroke-miterlimit="10" cx="37.25" cy="139.157" rx="32.25" ry="32.5"/>
<path class="path1" fill="none" stroke="#EC1177" stroke-width="10" stroke-miterlimit="10" d="M92,115.801c0,0,27-10.5,23-30.5s-39.5,7-33.5-27.5
	"/>
<path class="path2" fill="none" stroke="#0EA8DC" stroke-width="10" stroke-miterlimit="10" d="M182.005,115.051c0,0,33.983-20.729,28.949-60.211
	c-5.035-39.482-49.718,13.819-42.166-54.289"/>
<path class="path3" fill="none" stroke="#A1C434" stroke-width="10" stroke-miterlimit="10" d="M132.948,115.051c0,0,30.904-16.293,26.325-47.328
	c-4.578-31.035-45.21,10.862-38.343-42.672"/>
</svg>
								</div>
								<ul class="grid">
								</ul>
								<div class="row">
									<!-- <div class="result-container col-md-6">
									</div> -->
									<div class="pagination-container col-md-6">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</main>