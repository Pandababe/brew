<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
  global $post;
  $post_slug=$post->post_type;

?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>  style="padding-top: 100px;">
	<div class="intro-content">
		<h2><?php the_title(); ?></h2>
		<h3 style="color: #8f8f8f; font-size: 100%;"><?php get_field('sub_title'); ?></h3>
	</div>

	<?php if($post_slug == 'work') { ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php if(get_field('video')) { ?>
					<iframe class="embed-player slide-media" src="<?php echo get_field('video') ?>?autoplay=1&loop=1&title=0&byline=0&portrait=0&background=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="autoplay; fullscreen"></iframe>
				<?php } else { ?>
					<h1 class="no-vid"> No Video Uploaded </h1>
				<?php } ?>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				
					
			
						<div class="row">
							<div class="col-md-5">
							<div class="subpage-intro video">
								<h2><?php echo tax_item_name(get_the_ID(), 'director') ?></h2>
								<p>Director</p>
							</div>
							</div>
							<div class="col-md-5">
							<div class="subpage-intro video">
								<h2><?php echo tax_item_name(get_the_ID(), 'client') ?></h2>
								<p>Client</p>
							</div>
							</div>
							<div class="col-md-2">
							<div class="subpage-intro video">
								<h2><?php echo tax_item_name(get_the_ID(), 'agency') ?></h2>
								<p>Agency</p>
							</div>
							</div>
						</div>

						<div class="post-navigation">
							<div style="float: left;">
								<p style="margin-bottom: 5px;"><a href="/works" style="color: #0DA9DE; font-size: 14px;">Other Works</a></p>
							</div>
							<div style="float: right;">
								<p style="margin-bottom: 5px;" class="share-ctrl"><a href="#" style="color: #A9CE37; font-size: 14px; text-align: right;">Share</a></p>
								<div class="share-btn none"><?php the_content(); ?></div>
							</div>
						</div>
				<?php
				the_post_navigation( array(
					'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentysixteen' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Next post:', 'twentysixteen' ) . '</span> ' .
						'<span class="post-title">%title</span>',
					'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentysixteen' ) . '</span> ' .
						'<span class="screen-reader-text">' . __( 'Previous post:', 'twentysixteen' ) . '</span> ' .
						'<span class="post-title">%title</span>',
						// 'in_same_term' => true,
						'taxonomy' => 'works'
				
				) );
				?>	
				
			</div>
			
		
				<?php
				} else { ?>
					<div class="container">
						<div class="single-image"><?php the_post_thumbnail(); ?></div>
						<div class="single-content">
							<?php the_content(); ?>
							
						</div>
					</div>
					
				<?php } ?>
		
		</div>
	</div>
</article><!-- #post-## -->
<script>
	var vidUrl = '<?php echo get_field('video') ?>'.split("/"),
		vidUrl_id = vidUrl[vidUrl.length - 1];

	$('.share-ctrl a').click(function(e){
		e.preventDefault();

		$('.share-btn').toggleClass('none');
	});

</script>
<style>
	.share-btn {
		position: absolute;
		display: block;
		width: 175;
		right: 0;
	}
	.share-btn.none {
		display: none;
	}
	.post-navigation a {
		padding: 1em 0;
	}
	.addtoany_content {
		margin: 10px 0;
	}
	.a2a_kit a {
		padding: 10px 5px;
	}
</style>