<?php
/**Template Name: News
 * The template for displaying news pages
 *
 * Used to display news-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these news views, you may create a
 * new template file for each one. For example, tag.php (Tag news),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>
<div id="primary" class="">
    <main id="main" class="site-main" role="main" style="padding-top: 100px;">
        <div class="container">
            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="intro-content">
						<h2><?php the_title(); ?></h2>
					</div>
					<div class="filters news">
						<div class="custom-drop-wrap">
							<div id="drop_sort" class="drop-down__button">
								Sort by:
								<span class="drop-text">recent post</span>
								<span class="icon-download"></span>
							</div>
							<div class="custom-drop-content">
								<ul class="button-group" data-sort="time">
									<li><a href="" data-sort-direction="asc" data-sort-value="filedate">Recent post</a></li>
									<li><a href="" data-sort-direction="desc" data-sort-value="filedate">Old post</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="news-list col-md-12 col-sm-12 col-xs-12">
					<div class="row grid">
						<?php
							$the_query1 = new WP_Query( array('post_type' => 'news') );
									if ( $the_query1->have_posts() ) {
									while ( $the_query1->have_posts() ) {
										$the_query1->the_post();
						?>
						<article class="col-md-6 col-sm-6 col-xs-12 service-box" data-file-date="01012018">
							<div class="post_image">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail() ?>
									<span class="news-date">
										<?php echo get_the_date('M'); ?> <?php echo get_the_date('d'); ?>, <?php echo get_the_date('Y'); ?>
									</span>
								</a>
							</div>
							<div class="post_text">
								<div class="post_text_inner">
									<h3>
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h3>
									<p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 150, '...');?></p>
								</div>
							</div>

						</article>
						<?php }
						wp_reset_postdata();
						} ?>
						
					</div>
				</div>
            </div>
        </div>
    </main>
</div>

<?php get_footer() ?>