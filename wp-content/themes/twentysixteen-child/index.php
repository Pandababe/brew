<?php /* Template Name: Home Page */ ?>
<?php get_header(); 


?>
  <section class="main-slider">
    <?php
        $query = new WP_Query( array( 'post_type' => 'slider', 'orderby' => 'ASC') );
        if ( $query->have_posts() ) {
          while ( $query->have_posts() ) {
            $query->the_post();
            $cat_slug = tax_item_slug(get_the_ID(), 'slider');

            ?>  
              <div class="item <?php echo $cat_slug; ?>">

                <?php if ($cat_slug == "vimeo ") { ?>
                    <iframe class="embed-player slide-media" src="<?php echo the_field('source'); ?>?api=1&byline=0&portrait=0&title=0&background=1&mute=1&loop=1&autoplay=1" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                <?php } else if ($cat_slug == "youtube ") { ?>
                    <iframe class="embed-player slide-media" src="<?php echo the_field('source'); ?>?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1" width="980" height="520" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen allow="autoplay; fullscreen"></iframe>
                  <?php } else if ($cat_slug == "video ") { ?>
                    <video class="slide-video slide-media" loop muted autoplay preload="metadata" poster="">
                      <source src="<?php echo the_field('source'); ?>" type="video/mp4" />
                    </video>
                <?php } else { ?>
                    <span class="loading">Loading...</span>
                    <figure>
                      <div class="slide-image slide-media" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');">
                        <img data-lazy="<?php echo get_the_post_thumbnail_url(); ?>" class="image-entity" />
                      </div>

                    </figure>
                  
                 <?php } ?>
                <div class="caption">
                  <span><?php the_title(); ?></span>
                </div>
              </div>
          <?php }
          wp_reset_postdata();
        }
    ?>
  </section>
<?php
 $query = new WP_Query( array( 'post_type' => 'home_page','tag' => 'welcome-to-brew-productions') );
        if ( $query->have_posts() ) { 
          while ( $query->have_posts() ) {
            $query->the_post(); ?>  
<div class="intro-wrapper">
    <?php
        if ( function_exists( 'the_custom_logo' ) ) {
                        the_custom_logo();
    } ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="intro-content">
                    <!-- <h2><?php the_title(); ?></h2>
                    <p>
                      <?php the_content(); ?>
                    </p> -->
                    <?php if(get_field('custom_links')) { ?>
                          <a href="<?php echo the_field('custom_links'); ?>"><?php the_field('link_text'); ?></a><br>
                      <?php } else { ?>
                          <a href="<?php echo the_permalink(); ?>"><?php the_field('link_text'); ?></a><br> &nbsp;
                      <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
    wp_reset_postdata();
  }
 $query = new WP_Query( array( 'post_type' => 'home_page','tag' => 'what_we_do') );
    if ( $query->have_posts() ) { $query->the_post(); ?>  
    <!-- <div class="services-wrapper" style="background: rgb(17,17,17);">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="intro-content">
                        <h2><?php the_title(); ?></h2>
                        <p><?php the_content(); ?></p>
                    </div>
                    <div class="services-grid">
                        <ul>
                            <?php
                              $terms = get_terms( array(
                                  'taxonomy' => 'works',
                                  'hide_empty' => false,
                              ) );
                             foreach( $terms as $cat ) { ?>
                               <li class="service-box wow fadeInUp animated" data-wow-delay=".75s">
                                      <a href="<?php echo the_field('custom_links') .'?cat='. $cat->slug ?>">
                                        <img src="<?php echo ( z_taxonomy_image_url($cat->term_id) ) ? z_taxonomy_image_url($cat->term_id) : get_stylesheet_directory_uri(). '/images/placeholder.gif'; ?>">
                                      
                                        <div class="service-cap">
                                            <h3><?php echo $cat->name; ?></h3>
                                        </div>
                                      </a>
                                </li>

                             <?php 
                              }
                            ?>      
                        </ul>
                    </div>
                   
                </div>
                <div class="col-md-12">
                    <div class="more-vids">
                      <?php if(get_field('custom_links')) { ?>
                          <a href="<?php echo the_field('custom_links'); ?>"><?php the_field('link_text'); ?></a>
                      <?php } else { ?>
                          <a href="<?php echo the_permalink(); ?>"><?php the_field('link_text'); ?></a>
                      <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <?php get_template_part( 'template-parts/content', 'works' ); 
    wp_reset_postdata();
  }
get_footer(); ?>