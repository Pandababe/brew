<?php
	/* 
		Template Name: Works
	*/
	get_header();
?>

<div id="primary" class="">
	<main id="main" class="site-main" role="main" style="padding-top: 100px;">
<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="intro-content">
							<h2>Our Works</h2>
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="filters">
							<div class="custom-drop-wrap">
								<div id="drop_category" class="drop-down__button">
									What We Do:
									<span class="drop-text">all</span>
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content" id="cat-works">
									<ul data-filter-group="category" class="button-group">
										<?php echo cat_terms('works') ?>
									</ul>
								</div>
							</div>
							<div class="custom-drop-wrap">
								<div id="drop_director" class="drop-down__button">
									Director: 
									<span class="drop-text">all</span>
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content">
									<ul data-filter-group="director" class="button-group">
										<?php echo cat_terms('director') ?>
									</ul>
								</div>
							</div>
							<div class="custom-drop-wrap">
								<div id="drop_sort" class="drop-down__button">
									<span class="drop-text">recent work</span>
									<span class="icon-download"></span>
								</div>
								<div class="custom-drop-content">
									<ul class="button-group" data-sort="time">
										<li data-filter="DESC" class="active">Recent work</li>
										<li data-filter="ASC">Old work</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="services-wrapper works-page">
							<div class="services-grid">
							<svg class="lds-blocks" width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;"><rect x="19" y="19" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0s" calcMode="discrete"></animate>
</rect><rect x="40" y="19" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.125s" calcMode="discrete"></animate>
</rect><rect x="61" y="19" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.25s" calcMode="discrete"></animate>
</rect><rect x="19" y="40" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.875s" calcMode="discrete"></animate>
</rect><rect x="61" y="40" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.375s" calcMode="discrete"></animate>
</rect><rect x="19" y="61" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.75s" calcMode="discrete"></animate>
</rect><rect x="40" y="61" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.625s" calcMode="discrete"></animate>
</rect><rect x="61" y="61" width="20" height="20" fill="#717171">
  <animate attributeName="fill" values="#ed1278;#717171;#717171" keyTimes="0;0.125;1" dur="1s" repeatCount="indefinite" begin="0.5s" calcMode="discrete"></animate>
</rect></svg>
								<ul class="grid">
								</ul>
								<div class="row">
									<div class="result-container col-md-6">
									</div>
									<div class="pagination-container col-md-6">
									</div>
								</div>
								<p class="no-results" style="display: none; font-size: 100px; color: rgba(254,254,254,0.6);">No Result Found</p>
							</div>
						</div>
					</div>
				</div>
			</div>
	</main>
</div>
<?php get_footer(); ?>