<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>
<div id="primary" >
	<main id="main" class="site-main" role="main" style="padding-top: 100px;">
      <div class="intro-content">
	  <h2> 
		<?php echo get_queried_object()->name; ?>
	</h2>
      </div>
      <div class="container">
       <div class="row">
        <?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post(); ?>
			
      <div class="col-md-3 col-sm-4 col-xs-6 search-item">
	  <?php
		$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large'); 
		if($image == null) {
			$image_src = site_url() . '/wp-content/uploads/2018/05/placeholder.gif';
		} else {
			$image_src = $image[0];
		}
		 ?>
        <a href="<?php the_permalink(); ?>" class="search-img"><img src="<?php echo $image_src; ?>" alt=""></a>
        <p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 95, '...'); ?></p>

        <div class="subpage-intro search">
          <h2><a href="<?php echo esc_url( get_permalink() )?>" rel="bookmark"><?php echo the_title() ?></a></h2>
        </div>
						
      </div>
				
			<?php
				endwhile; ?>
      </div>
      </div>
				<?php
			else :
				echo '<header class="page-header" style="color: #fff; width: fit-content; display: table; margin: 5em auto;"><h1 class="page-title">Nothing Found</h1></header>';
				
			endif;
		
				
				


		?>
	</main><!-- .site-main -->
</div><!-- .content-area -->
<?php get_footer() ?>
