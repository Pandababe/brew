<?php
	/* 
		Template Name: Works
	*/
	get_header();
?>

<div id="primary" style="padding-top: 100px;">
 
<?php get_template_part( 'template-parts/content', 'works' );  ?>
<main id="main" class="site-main" role="main">
</main>
</div>
<?php get_footer(); ?>