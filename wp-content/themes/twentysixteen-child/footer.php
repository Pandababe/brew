 <?php 
  $query = new WP_Query( array( 'post_type' => 'home_page','category_name' => 'contacts') );
    if ( $query->have_posts() ) { $query->the_post(); ?>  
    <div class="footer-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="intro-content">
                        <h2><?php the_title(); ?></h2>
                    </div>
                    <div class="footer-content">
                        <span class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon1.gif'; ?>" alt=""></span>
                        <p><?php echo the_field('address') ?></p>
                        <span class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon2.gif'; ?>" alt=""></span>
                        <p><?php echo the_field('numbers') ?></p>
                        <span class="rotate"><img src="<?php echo get_stylesheet_directory_uri(). '/images/icon3.gif'; ?>" alt=""></span>
                        <p><?php echo the_field('fax') ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php 
    wp_reset_postdata();
  } 

 $query = new WP_Query( array( 'post_type' => 'home_page','tag' => 'credits') );
    if ( $query->have_posts() ) { $query->the_post(); ?>  

    <div class="top-social sp-only">
      <?php if ( has_nav_menu( 'social' ) ) : 
        wp_nav_menu( array(
          'theme_location' => 'social',
          'menu_class'     => 'nav-menu',
        ) );
      endif; ?>
    </div>
    <div class="lemon">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p><?php the_content(); ?> 
                        <?php if(get_field('custom_links')) { ?>
                          <a href="<?php echo the_field('custom_links'); ?>"><?php the_field('link_text'); ?></a>
                      <?php } else { ?>
                          <a href="<?php echo the_permalink(); ?>"><?php the_field('link_text'); ?></a>
                      <?php } ?>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <?php 
    wp_reset_postdata();
  } wp_footer(); ?>
</body>
</html>