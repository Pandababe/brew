<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
	<div id="primary" style="padding-top: 100px">
		<main id="main" role="main" class="not-found-wrap">
			<div class="col-sm-12 text-center"><img alt="Not Found" src="<?php echo get_stylesheet_directory_uri() ?>.'/images/404.png'; ?>">
				<div class="not-found-txt">
				<h1>Sorry!</h1>
				<p>The page you are looking for does not exist or another error occurred</p>
				<a href="<?php echo site_url() ?>"><em class="fa fa-fw"></em> &nbsp; GO BACK HOME</a></div>
			</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
<?php get_footer(); ?>
