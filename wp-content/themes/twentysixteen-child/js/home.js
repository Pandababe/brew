jQuery(document).ready(function () {

  var $ = jQuery,
    $window = $(window),
    $document = $(document),
    docHeight = $document.innerHeight(),
    winWidth = $window.innerWidth(),
    winHeight = $window.innerHeight(),
    scrolled = $(window).scrollTop(),
    siteurl = '<?php site_url ?>';

  

  /**
   * --------------------------------------------------------------------------
   * HIDE P TAGS WITH SPACES ONLY
   * --------------------------------------------------------------------------
   */
  $('p').each(function () {
    if ($.trim($(this).text()) == '' && $(this).children().length == 0) {
      $(this).hide();
    }
  });

  /**
   * --------------------------------------------------------------------------
   * EVENTS
   * --------------------------------------------------------------------------
   */

  $window.on('resize', function () {
    updateOnResize();
  });

  $window.on('scroll', function () {
    backTop();
  });

  $(window).on("resize.slickVideoPlayer", function () {
    resizePlayer(iframes, 16 / 9);
  });

  var updateOnResize = debounce(function () {
    updateValueOnResize();
    updateStyleOnResize();
  }, 250);

  function updateValueOnResize() {
    winWidth = $window.innerWidth();
    winHeight = $window.innerHeight();
    services_wrap = $(".services-grid ul").innerWidth();
    service_col = $(".services-grid ul li").innerWidth();
    service_box = $(".service-box a img");
    sboxHeight = service_box.innerHeight();
    
      servicesHeight();
    
    searchAuto();
  }

  function updateStyleOnResize() {
    // var services_wrap = $(".services-grid ul").innerWidth(),
    //   service_col = $(".service-box");

    // service_col.css('width', 0);
    // service_col.css('width', services_wrap / 3 - 10);

    // if (winWidth <= 991) {
    //   service_col.css('width', 0)
    //   service_col.css('width', services_wrap / 2)
    // } else if (winWidth <= 640) {
    //   service_col.css('width', 0)
    //   service_col.css('width', services_wrap)
    // }
  }

  function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
        args = arguments;
      var later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  /**
   * --------------------------------------------------------------------------
   * BACK TO TOP
   * --------------------------------------------------------------------------
   */

  function backTop() {
    if ($(this).scrollTop()) {
      $('#backTop').fadeIn();
    } else {
      $('#backTop').fadeOut();
    }
  }

  $("#backTop").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 500);
  });

  /**
   * --------------------------------------------------------------------------
   * MENU
   * --------------------------------------------------------------------------
   */

  $(".menu-icon").on("click", function () {
    $(this).stop(true, true).toggleClass("open");
    $(".overlay").stop(true, true).toggleClass("show");
  });

  /**
   * --------------------------------------------------------------------------
   * SLIDER
   * --------------------------------------------------------------------------
   */

  // Initialize
  slideWrapper.on("init", function (slick) {
    slick = $(slick.currentTarget);
    setTimeout(function () {
      playPauseVideo(slick, "play");
    }, 1000);
    resizePlayer(iframes, 16 / 9);
    // var currentSlide = slick.find(".slick-current");
    // currentSlide.find("iframe").attr("allow", "")
    
  });
  slideWrapper.on("beforeChange", function (event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick, "pause");
  });
  slideWrapper.on("afterChange", function (event, slick) {
    slick = $(slick.$slider);
    playPauseVideo(slick, "play");
  });
  slideWrapper.on("lazyLoaded", function (event, slick, image, imageSource) {
    lazyCounter++;
    if (lazyCounter === lazyImages.length) {
      lazyImages.addClass('show');
      slideWrapper.slick("slickPlay");
    }
  });
  
  //start the slider
  slideWrapper.slick({
    // fade:true,
    autoplaySpeed: 4000,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    dots: true,
    infinite:false,
    cssEase: "cubic-bezier(0.87, 0.03, 0.41, 0.9)",
    prevArrow: false,
    nextArrow: false
    // prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button"></button>',
    // nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button"></button>',
  });
  /**
   * --------------------------------------------------------------------------
   * SERVICES AUTOHEIGHT
   * --------------------------------------------------------------------------
   */

  function servicesHeight() {
    var service_box = $(".service-box a img");
    service_box.css('min-height', '');
    var sboxHeight = service_box.innerHeight();
    service_box.css('max-height', sboxHeight);
    if (winWidth <= 640) {
      service_box.css({ 'min-height': 'inherit', 'width': '100%' });
      console.log("less than 640")
    } else if (winWidth <= 991) {
      service_box.css('min-height', '');
      service_box.autoHeight({ column: 2, clear: 2 });
      console.log("less than 991")
    } else if (winWidth >= 992) {
      service_box.css('min-height', '');
      service_box.autoHeight({ column: 3, clear: 3 });
      console.log("greater than 992")
    }
    console.log('autoheight triggerred');
   }

   /**

   * --------------------------------------------------------------------------
   * TOGGLE SEARCH
   * --------------------------------------------------------------------------
   */

  var search_icon = $(".nav-menu li a i").find(".fa-search");
  $(".nav-menu li").find(".fa-search").closest("li").attr({
    "data-target": "#searchModal",
    "data-toggle": "modal"
  });


  


  /**
   * --------------------------------------------------------------------------
   * TOGGLE DROPDOWN IN SELECT IN WORKS PAGE
   * --------------------------------------------------------------------------
   */
  $('div[id^="drop_"]').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).closest('.custom-drop-wrap').toggleClass('drop-down--active');
    $(this).parent().addClass('active').siblings().removeClass('drop-down--active active');
  });

  $('.custom-drop-content ul li').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var clicked = $(this).text(),
        data_all = $(this).attr("data-all");

    if (data_all == 'director') {
      clicked = "DIRECTORS";
    } else if (data_all == 'works') {
      clicked = "OUR WORK";
    }
    $(this).parents('.custom-drop-wrap.active').find(".drop-text").text(clicked);
    // $(this).closest('.custom-drop-wrap').removeClass('drop-down--active');
    $('.no-results').hide();
    $('.child').css('display', 'none');
  });

  $('.custom-drop-content ul li, div[id^="drop_"]').on('click', function (e) {
    e.stopPropagation();
    $('.child').css('display', 'none');
  });

  $(document).click(function (e) {
    $(".custom-drop-wrap").removeClass("drop-down--active");
    $('.child').css('display', 'none');
    $('.grid').css('margin-top', '0');
  });


    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var cat = getUrlParameter('cat');

    if(cat) {
        var category = $('#cat-works ul li').each(function(){
          var items = $(this).data('filter'),
            same = "",
            text = "";
          if (items == cat) {
            same = items;
            var data = {};
            $('#drop_category .drop-text').text($(this).data('filter', same).text());
            $(this).addClass('active').siblings().removeClass('active');
            data['cat_slug'] = same;
            data['dir_slug'] = "";
            data['sort_slug'] = "DESC";
            ajax_filter(data);
            console.log(data);
          } else {
            same = "";
          }
        });
    } else {
      var data = {};
      data['cat_slug'] = "";
      data['dir_slug'] = "";
      data['sort_slug'] = "DESC";
      ajax_filter(data);
      console.log(data);
    }

    $('body').delegate('.pagination-container .page-numbers','click',function(e) {
      e.preventDefault();
        $('document').scrollTop(0);
        var paged = $(this).attr('href').split('=')[1];
        var data = {};
        var cat = $('#cat-works li.active').data('filter');
        data['cat_slug'] = cat;
        if (paged) {
          
          data['paged'] = paged;
        } else {
          data['paged'] = 1;
        }

        ajax_filter(data);
    });

  

    $('.custom-drop-content ul li').click(function() {
      var slug = $(this).data("filter"),
          arr = [],
          data = {};
      
      $(this).addClass('active').siblings().removeClass('active');
      $(this).parent().find('.child-cat li').removeClass('active');
      $(this).parents('.child-cat').siblings().removeClass('active');
      $('.custom-drop-content ul li.active').each(function(index, value){
        arr.push($(value).data('filter'));
      });
   
        
        data['cat_slug'] = arr[2];
        data['dir_slug'] = arr[1];
        data['sort_slug'] = arr[0];

        ajax_filter(data);
        servicesHeight();

      // alert("success")
      console.log(data);
      console.log(arr.length);
      console.log(arr);

    });

    $('#drop_director-parent').click(function(){
      $('.custom-drop-content.child-cat').css('border-top', '1px solid #939393');
      $('.child').css('display', 'inline-block');
      var height = $('.custom-drop-content').height() + $('.child-cat').height();
      $('.grid').css('margin-top', height + 'px');
    });

    $('.drop-down__button, .child, .parent').click(function(){
      var height = $('.custom-drop-content').height() + $('.child-cat').height();
      $('.grid').css('margin-top', height + 'px');
    });

    // $('.parent').click(function(){
    //   $('.child').css('display', 'none');
    //   $('.grid').css('margin-top', '0');
    // });


    function ajax_filter(data){
      $('.services-grid .grid').find('li').remove();
      $('.lds-blocks').show();
      $.post(url + '/jaxjaxjaxjaxbywakawaku/', {
        action: 'ajax_filter',
        post_data: data,
        success: function(){
          
            servicesHeight();
         
        },
      }, function(data){
            var returnedData = JSON.parse(data),
            item = returnedData.data,
            pagination = returnedData.pagination,
            count = item.length,
            html = "",
            result = "";
            if (pagination == null) {
              pagination_html = '';
            } else {
              pagination_html = '<div class="pagination">'+ pagination +'</div>';
            }

            if (item.success) {
                html = '<h2 class="no-results">' + item.success + '</h2>';
            } else {
                for( x=0; x < count; x++ ) {
                  html +=        '<li class="service-box" data-wow-delay=".75s">';
                  html +=         '<a href="'+ item[x].post_link +'">';
                  html +=            '<img src="'+ item[x].post_img_src +'" alt="">';
                  html +=            '<div class="service-cap"><h3>'+ item[x].post_title +'</h3><h4>'+ item[x].post_sub +'</h4></div>';
                  html +=          '</a>';
                  html +=        '</li>';
                }
            }

            if (returnedData.post_displayed == null || returnedData.post_count == null) {
              result = "";
            } else {
              result = 'Result '+ returnedData.post_displayed +' of '+ returnedData.post_count;
            }
            $('.lds-blocks').hide();
            $('.services-grid .grid').html(html);
            $('.pagination-container').html(pagination_html);
            $('.result-container').html(result);
      });
    }


  /**
   * --------------------------------------------------------------------------
   * ABOUT US PAGE HOVER EFFECT
   * --------------------------------------------------------------------------
   */

  function initCoverBoxes() {
    if ($(".cover_boxes").length) {
      $(".cover_boxes").each(function () {
        var active_element = 0;
        var data_active_element = 0;
        if (
          typeof $(this).data("active-element") !== "undefined" &&
          $(this).data("active-element") !== false
        ) {
          data_active_element = parseFloat($(this).data("active-element"));
          active_element = data_active_element - 2;
        }
        var number_of_columns = 3;
        if (
          typeof $(this).data("active-element") !== "undefined" &&
          $(this).data("active-element") == 2
        ) {
          number_of_columns = 2;
        }
        active_element =
          data_active_element > number_of_columns ? 0 : active_element;
        $(this).find("li").eq(active_element).addClass("act");
        $('.team-image').eq(active_element).addClass('act')

        

        var cover_boxed = $(this);
        $(this).find("li").each(function () {
            $(this).hover(function () {
              $(cover_boxed).find("li").removeClass("act");
              $(this).addClass("act");
              var index = $(this).index();
              $('.team-image').eq(index).addClass('act').siblings().removeClass('act');
            });
          });
      });
    }
  }

    /**
   * --------------------------------------------------------------------------
   * SEARCH AUTO HEIGHT
   * --------------------------------------------------------------------------
   */

  function searchAuto() {
    $(".search-item .search-img").css('height','');
    $(".search-item .search-img").autoHeight();
  }


 /**
  * --------------------------------------------------------------------------
  * ONLOAD FUNCTIONS
  * --------------------------------------------------------------------------
  */
 servicesHeight();
 initCoverBoxes();
 searchAuto();

    /**
   * --------------------------------------------------------------------------
   * SINGLE PAGE VIDEO IFRAME RESPONSIVE
   * --------------------------------------------------------------------------
   */

  if($("article").length) {
    $("article iframe").css({
      'width' : winWidth
    });
    var iframe = document.querySelector('iframe'),
          options = {
            id: vidUrl_id,
            loop: true,
            muted: false,
            autoplay: true,
          },
          player = new Vimeo.Player(iframe, options);
    player.ready().then(function () {
      // player.setVolume(0);
    });
  }

  /**
   * --------------------------------------------------------------------------
   * SERVICE GRID
   * --------------------------------------------------------------------------
   */
    // var services_wrap = $(".services-grid ul").innerWidth(),
    //   service_col = $(".service-box");
    
    // service_col.css('width', 0);
    // service_col.css('width', services_wrap / 3-10);

    // if (winWidth <= 991) {
    //   service_col.css('width', 0)
    //   service_col.css('width', services_wrap/2)
    // } else if (winWidth <= 640) {
    //   service_col.css('width', 0)
    //   service_col.css('width', services_wrap)
    // }



});

/**
 * --------------------------------------------------------------------------
 * SLIDER
 * --------------------------------------------------------------------------
 */
var slideWrapper = $(".main-slider"),
  iframes = slideWrapper.find('.embed-player'),
  lazyImages = slideWrapper.find('.slide-image'),
  lazyCounter = 0;

// POST commands to YouTube or Vimeo API
function postMessageToPlayer(player, command) {
  if (player == null || command == null) return;
  player.contentWindow.postMessage(JSON.stringify(command), "*");
}



// When the slide is changing
function playPauseVideo(slick, control) {
  var currentSlide, slideType, startTime, player, video;

  currentSlide = slick.find(".slick-current");
  slideType = currentSlide.attr("class").split(" ")[1];
  player = currentSlide.find("iframe").get(0);
  startTime = currentSlide.data("video-start");


  if (slideType === "vimeo") {
    switch (control) {
      case "play":
        if ((startTime != null && startTime > 0) && !currentSlide.hasClass('started')) {
          currentSlide.addClass('started');
          postMessageToPlayer(player, {
            "method": "setCurrentTime",
            "value": startTime
          });
        }
        postMessageToPlayer(player, {
          "method": "play",
          "value": 1
        });
        break;
      // case "pause":
      //   postMessageToPlayer(player, {
      //     "method": "pause",
      //     "value": 1
      //   });
      //   break;
    }
  } else if (slideType === "youtube") {
    switch (control) {
      case "play":
        postMessageToPlayer(player, {
          "event": "command",
          "func": "mute"
        });
        postMessageToPlayer(player, {
          "event": "command",
          "func": "playVideo"
        });
        break;
      case "pause":
        postMessageToPlayer(player, {
          "event": "command",
          "func": "pauseVideo"
        });
        break;
    }
  } else if (slideType === "video") {
    video = currentSlide.children("video").get(0);
    if (video != null) {
      if (control === "play") {
        video.play();
      } else {
        video.pause();
      }
    }
  }
}

// Resize player
function resizePlayer(iframes, ratio) {
  if (!iframes[0]) return;
  var slider = $(".main-slider"),
    width = slider.innerWidth(),
    playerWidth,
    height = slider.innerHeight(),
    playerHeight,
    ratio = ratio || 16 / 9;

  iframes.each(function () {
    var current = $(this);
    if (width / ratio < height) {
      playerWidth = Math.ceil(height * ratio);
      current.width(playerWidth).height(height).css({
        left: (width - playerWidth) / 2,
        top: 0
      });
    } else {
      playerHeight = Math.ceil(width / ratio);
      current.width(width).height(playerHeight).css({
        left: 0,
        top: (height - playerHeight) / 2
      });
    }
  });
}