<?php

function my_theme_enqueue_styles() {

	// Basic Import
    $parent_style = 'parent-style';
	wp_enqueue_style( 'slick_css',get_stylesheet_directory_uri() . '/css/slick.css');
	wp_enqueue_style( 'slick_theme_css',get_stylesheet_directory_uri() . '/css/slick-theme.css');
	wp_enqueue_style( 'font_awesome_css',get_stylesheet_directory_uri() . '/css/font-awesome.css');
	wp_enqueue_style( 'icomoon_css',get_stylesheet_directory_uri() . '/fonts/icomoon/style.css');
    wp_enqueue_style( 'animate_css', get_stylesheet_directory_uri() . '/css/animate.css' );
    wp_enqueue_style( 'bootstrap_css', get_stylesheet_directory_uri() . '/css/bootstrap.css' );
    wp_enqueue_style( 'bootstrap_grid_css', get_stylesheet_directory_uri() . '/css/bootstrap-grid.css' );
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/custom.css', array( $parent_style ), wp_get_theme()->get('Version'));
    
    // Upgrade Jquery
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-2.2.4.js", array(), '3.1.1' );
    wp_enqueue_script( 'vimeo_js', "https://player.vimeo.com/api/player.js", array( 'jquery' ), '1.0', true );	
    wp_enqueue_script( 'slick_js', get_stylesheet_directory_uri() . '/js/slick.js', array( 'jquery' ), '1.0', true );	
    wp_enqueue_script( 'bootstrap_js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '1.0', true );	
    wp_enqueue_script( 'autoheight_js', get_stylesheet_directory_uri() . '/js/autoheight.js', array( 'jquery' ), '1.0', true );	
    wp_enqueue_script( 'isotope_js', get_stylesheet_directory_uri() . '/js/isotope.js', array( 'jquery' ), '1.0', true );   
    wp_enqueue_script( 'masonry_js', 'https://unpkg.com/masonry-layout@4.2.1/dist/masonry.pkgd.min.js', array( 'jquery' ), '1.0', true );	
    wp_enqueue_script( 'home_js', get_stylesheet_directory_uri() . '/js/home.js', array( 'jquery' ), '1.0', true );	

}


add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
add_theme_support( 'after_setup_theme');
add_theme_support( 'custom-logo' );


function cat_terms($slug) {

    $html = "";
    $terms = get_terms([
        'taxonomy' => $slug,
        'hide_empty' => false,
        'orderby'  => 'id',
        'order'    => 'ASC',
        'parent' => 0
    ]);
    
    foreach($terms as $cat) {
        $term_children = get_term_children($cat->term_id, $slug);
        if ( $cat->slug != 'other-directors' ) {
            $html .= 
            '<li id="'. ($term_children ? 'drop_director-parent' : '') .'"
            data-filter="'.$cat->slug.'" 
            class="'. ($term_children ? '' : 'parent') .' button">' .$cat->name. '
            </li>';
        }
    }
    $html .= '<li id="drop_director-parent" data-filter="other-directors" class=" button">Other Directors</li>';
    $html .= '<li data-filter="" data-all="'.$slug.'" class="active button">ALL</li>';
    $child = get_categories( array(
        'type'        => 'post',
        'child_of'    => 124,
        'parent'      => '',
        'orderby'     => 'id',
        'order'       => 'ASC',
        'hide_empty'  => false,
        'hierarchical'=> 1,
        'exclude'     => '',
        'include'     => '',
        'number'      => '',
        'taxonomy'    => $slug,
        'pad_counts'  => false 
        ) );
        if($child) {
        $html .= '<div class="custom-drop-content child-cat" style="margin-top: 0;"><ul>';   
            foreach($child as $item) {
                $html .= '<li class="child" data-filter="' .$item->slug. '" class="button">' .$item->name. '</li>';
            }
        $html .= '</div><ul>';     
    }
    
    return $html;
}

function taxonomy_name_this($id, $slug) {
    $result = "";
    $ctax = get_the_terms( $id, $slug);
    if ($ctax) {
        foreach($ctax as $item)  {
            $result .=  $item->name;
        }
    } else {
        $result = "";
    }
    return $result;
}

function tax_item_name($postid, $termslug) {
        $result = "";
        $terms_dir = get_the_terms( $postid, $termslug );
        if ($terms_dir) {
          foreach( $terms_dir as $cat) {
            if($cat->name != 'Other Directors') {
                $result .= '<a href="/'. $termslug .'/'  . tax_item_slug($postid, $termslug) .'">' . $cat->name . '</a>';
            }  
          }
        } else {
           $result = "N/A";
        }
            
        return $result;
}

function tax_item_slug($postid, $termslug) {
        $result = "";
            $terms_dir = get_the_terms( $postid, $termslug );
        if($terms_dir) {
            foreach( $terms_dir as $cat) {
                $result .= $cat->slug . ' ';
            }
        } else {
          $result = "";
        }
        return $result;
}

/****** Add Thumbnails in Manage Posts/Pages List ******/
if ( !function_exists('AddThumbColumn') && function_exists('add_theme_support') ) {

    // for post and page
    add_theme_support('post-thumbnails', array( 'post', 'page' ) );
    
    function AddThumbColumn($cols) {
    
    $cols['thumbnail'] = __('Thumbnail');
    
    return $cols;
    }
    
    function AddThumbValue($column_name, $post_id) {
        $width = (int) 60;
        $height = (int) 60;
    
        if ( 'thumbnail' == $column_name ) {
            // thumbnail of WP 2.9
            $thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
            // image from gallery
            $attachments = get_children( array('post_parent' => $post_id, 'post_type' => 'attachment', 'post_mime_type' => 'image') );
            if ($thumbnail_id)
                $thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
            elseif ($attachments) {
            foreach ( $attachments as $attachment_id => $attachment ) {
                $thumb = wp_get_attachment_image( $attachment_id, array($width, $height), true );
            }
            }
            if ( isset($thumb) && $thumb ) {
                echo $thumb;
            } else {
                echo __('None');
            }
        }
    }
    
    // for posts
    add_filter( 'manage_posts_columns', 'AddThumbColumn' );
    add_action( 'manage_posts_custom_column', 'AddThumbValue', 10, 2 );
    
    // for pages
    add_filter( 'manage_pages_columns', 'AddThumbColumn' );
    add_action( 'manage_pages_custom_column', 'AddThumbValue', 10, 2 );
    }
    if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'single-post-medium', 515 );
        add_image_size( 'single-post-small', 250 );
    }
    
    add_filter( 'image_size_names_choose', 'my_custom_sizes' );
    
    function my_custom_sizes( $sizes ) {
        return array_merge( $sizes, array(
            'single-post-medium' => __('Your Medium Size Name'),
            'single-post-small' => __('Your Small Size Name'),
        ) );
    }
    @ini_set( 'upload_max_size' , '64M' );
    @ini_set( 'post_max_size', '64M');
    @ini_set( 'max_execution_time', '300' );

    function csv_menu() {
        add_menu_page(
         "CSV Upload",
         "CSV Upload",
         0,
         "csv-menu-slug",
         "csv_options"
         );
    }

    function csv_options() {
    
        if(isset($_POST['submit'])) {
            if ( ! function_exists( 'wp_handle_upload' ) ) {
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
            }
            $uploadedfile = $_FILES['fileToUpload'];
            $upload_overrides = array( 'test_form' => false );
            $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
            $csv = Array();
            $rowcount = 0;
            if (($handle = fopen($movefile['url'], "r")) !== FALSE) {
                $max_line_length = defined('MAX_LINE_LENGTH') ? MAX_LINE_LENGTH : 10000;
                $header = fgetcsv($handle, $max_line_length);
                $header_colcount = count($header);
                while (($row = fgetcsv($handle, $max_line_length)) !== FALSE) {
                    $row_colcount = count($row);
                    if ($row_colcount == $header_colcount) {
                        $entry = array_combine($header, $row);
                        $csv[] = $entry;
                    }
                    else {
                        error_log("csvreader: Invalid number of columns at line " . ($rowcount + 2) . " (row " . ($rowcount + 1) . "). Expected=$header_colcount Got=$row_colcount");
                        return null;
                    }
                    $rowcount++;
                }
                fclose($handle);
            }
            else {
                error_log("csvreader: Could not read CSV");
                return null;
            }
            
            foreach( $csv as $item ) {

           
                $name = $item['title'];
                $video_link = $item['video link'];
    
                $date = $item['date'];
    

                $director = $item['director'];
                $agency = $item['agency'];
                $category = $item['category'];
                $client = $item['client'];
    
    
                $post_id = wp_insert_post(array (
                    'post_type' => 'work',
                    'post_title' => $name,
                    'post_status' => 'publish'
                ));

                wp_set_object_terms( $post_id, $director, 'director' );
                wp_set_object_terms( $post_id, $agency, 'agency' );
                wp_set_object_terms( $post_id, $client, 'client' );
                wp_set_object_terms( $post_id, $category, 'works' );
         
                add_post_meta( $post_id, 'date', $date );
                add_post_meta( $post_id, 'video', $video_link );

            }
        }
        ?>
        <form action="" method="post" enctype="multipart/form-data">
            Select image to upload:
            <input type="file" name="fileToUpload" id="fileToUpload">
            <input type="submit" value="Upload CSV" name="submit">
        </form>
    
    <?php }
    
    add_action( 'admin_menu', 'csv_menu' );

?>