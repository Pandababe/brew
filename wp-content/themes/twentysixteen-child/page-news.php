<?php
/**Template Name: News
 * The template for displaying news pages
 *
 * Used to display news-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these news views, you may create a
 * new template file for each one. For example, tag.php (Tag news),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>
<div id="primary" class="">
    <main id="main" class="site-main" role="main" style="padding-top: 100px;">
        <div class="container">
            <div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="intro-content">
						<h2><?php the_title(); ?></h2>
					</div>
					<!-- <div class="filters news">
						<div class="custom-drop-wrap">
							<div id="drop_sort" class="drop-down__button">
								Sort by:
								<span class="drop-text">recent post</span>
								<span class="icon-download"></span>
							</div>
							<div class="custom-drop-content">
								<ul class="button-group">
									<li><a href="?sort=ASC">Recent post</a></li>
									<li><a href="?sort=DESC">Old post</a></li>
								</ul>
							</div>
						</div>
					</div> -->
				</div>
				<div class="news-list col-md-12 col-sm-12 col-xs-12">
					<div class="row grid">
						<?php
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$the_query1 = new WP_Query(
								array('post_type' => 'new',
								'posts_per_page' => 6,
								'paged' => $paged
								) );
									if ( $the_query1->have_posts() ) {
									while ( $the_query1->have_posts() ) {
										$the_query1->the_post();
						?>
						<article class="col-md-6 col-sm-6 col-xs-12" data-file-date="01012018">
							<div class="post_image">
								<a href="<?php the_permalink(); ?>">
									<?php the_post_thumbnail() ?>
									
								</a>
							</div>
							<div class="post_text">
								<div class="post_text_inner">
									<h3>
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h3>
									<p><?php $content = get_the_content(); echo mb_strimwidth($content, 0, 150, '...');?></p>
								</div>
							</div>

						</article>
						<?php
							}
						?>

						<div class="pager-news">
						<?php
							echo paginate_links( array(
								'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
								'total'        => $the_query1->max_num_pages,
								'current'      => max( 1, get_query_var( 'paged' ) ),
								'format'       => '?paged=%#%',
								'show_all'     => false,
								'type'         => 'plain',
								'end_size'     => 2,
								'mid_size'     => 1,
								'prev_next'    => true,
								'prev_text'    => sprintf( '<i></i> %1$s', __( 'Newer News', 'text-domain' ) ),
								'next_text'    => sprintf( '%1$s <i></i>', __( 'Older News', 'text-domain' ) ),
								'add_args'     => false,
								'add_fragment' => '',
							) );
						 ?>
						</div>
						<?php	
						wp_reset_postdata();
							} ?>
	
					</div>
				</div>
            </div>
        </div>
    </main>
</div>

<?php get_footer() ?>