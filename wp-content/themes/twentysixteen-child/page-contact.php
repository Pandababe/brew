<?php
   /**
    * Template Name: contact Us
    * The template for displaying contact pages
    *
    * Used to display contact-type pages if nothing more specific matches a query.
    * For example, puts together date-based pages if no date.php file exists.
    *
    * If you'd like to further customize these contact views, you may create a
    * new template file for each one. For example, tag.php (Tag contact),
    * category.php (Category archives), author.php (Author archives), etc.
    *
    * @link https://codex.wordpress.org/Template_Hierarchy
    *
    * @package WordPress
    * @subpackage Twenty_Sixteen
    * @since Twenty Sixteen 1.0
    */
   get_header();
   ?>
<div id="primary" class="contact">
   <main id="main" class="site-main" role="main" style="padding-top: 100px;">
      <div class="container">
         <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="intro-content">
				<h2><?php the_title(); ?></h2>
			</div>
		</div>
		<?php
		$query = new WP_Query( array( 'post_type' => 'contactus', 'contacts' => 'intro-main') );
		if ( $query->have_posts() ) { 
			while ( $query->have_posts() ) {
			$query->the_post(); ?>  
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="subpage-intro">
				<?php the_content(); ?>
			</div>
		</div>
		<?php }
			wp_reset_postdata();
			}
		?>
         </div>
				<?php
 					$query = new WP_Query( array( 'post_type' => 'contactus', 'contacts' => 'contacts', 'order' => 'ASC') );
			        if ( $query->have_posts() ) { 
			          while ( $query->have_posts() ) {
			            $query->the_post(); ?>  
							<div class="subpage-intro contact text-center">
								<div class="custom_font_holder">
									<h3><?php the_title(); ?></h3>
									<?php the_content(); ?>
								</div>
							</div>
				<?php }
			          wp_reset_postdata();
			        }
			    ?>
      </div>
   </main>
</div>
<style>
	.footer-wrapper {
		display: none;
	}
</style>
<?php get_footer() ?>