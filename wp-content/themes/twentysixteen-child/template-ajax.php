<?php 
/*
Template Name: Ajax Request Page
*/

    $post_data = $_POST;

    if(isset($post_data['post_data'])){
        call_user_func($post_data['action'], $post_data['post_data']);
    }
    else {
        call_user_func($post_data['action']);   
    }

    function ajax_filter($arr_data) {
		$cat_slug = $arr_data['cat_slug'];
		$dir_slug = $arr_data['dir_slug'];
		$sort_slug = $arr_data['sort_slug'];
		$paged_id = $arr_data['paged'];
		$default_posts_per_page = get_option( 'posts_per_page' );
    	$ctr = 0;
        $item = array();
		$data = array();
		$paged = ( $paged_id ) ? $paged_id : 1;
		if ( $cat_slug == "" && $dir_slug == "" ) {
			$args = array(
				'post_type' => 'work',
				'order' => $sort_slug,
				'posts_per_page' => $default_posts_per_page,
				'paged' => $paged
			);
		} else if ( $cat_slug == "" ) {
			$args = array(
				'tax_query' => array(
					array(
					
						'taxonomy' => 'director',
						'field' => 'slug',
						'terms' => $dir_slug
					)	
				),
				'order' => $sort_slug,
				'posts_per_page' => $default_posts_per_page,
				'paged' => $paged
			);
		} else if ( $dir_slug == "" ) {
			$args = array(
				'tax_query' => array(
					array(
					
						'taxonomy' => 'works',
						'field' => 'slug',
						'terms' => $cat_slug
					)
				),
				'order' => $sort_slug,
				'posts_per_page' => $default_posts_per_page,
				'paged' => $paged
			);
		} else {
			$args = array(
				'tax_query' => array(
					'relation' => 'AND',
					array(
					
						'taxonomy' => 'works',
						'field' => 'slug',
						'terms' => $cat_slug,
						
					),
					array(
					
						'taxonomy' => 'director',
						'field' => 'slug',
						'terms' => $dir_slug
					)	
				),
				'order' => $sort_slug,
				'posts_per_page' => $default_posts_per_page,
				'paged' => $paged
			);
		}

		$the_query1 = new WP_Query( $args );
		if ( $the_query1->have_posts() ) {
			while ( $the_query1->have_posts() ) {
					$the_query1->the_post();
					$image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large'); 
					if($image == null) {
						$image_src = site_url() . '/wp-content/uploads/2018/05/placeholder.gif';
					} else {
						$image_src = $image[0];
					}
					

					$item[$ctr]['post_link'] = get_permalink();
					$item[$ctr]['post_img_src'] = $image_src;
					$item[$ctr]['post_title'] = get_the_title();
					if(get_field('sub_title')) {
						$item[$ctr]['post_sub'] = get_field('sub_title');
					} else {
						$item[$ctr]['post_sub'] = '';
					}
					
					$item[$ctr]['post_director'] = taxonomy_name_this(get_the_ID(), 'director');
					$item[$ctr]['post_client'] = taxonomy_name_this(get_the_ID(), 'client');
					$item[$ctr]['post_brand'] = taxonomy_name_this(get_the_ID(), 'brand');
					$item[$ctr]['post_agency'] = taxonomy_name_this(get_the_ID(), 'agency');
					$item[$ctr]['post_slug'] = $cat_slug;
					$item[$ctr]['post_content_exerpt'] = wp_trim_words( get_the_content(), 3, '...' );
					$ctr++;
			}

			$links = paginate_links( array(
				'base'         => '%_%',
				'total'        => $the_query1->max_num_pages,
				'current'      => max( 1, $paged_id ),
				'format'       => '?paged=%#%',
				'show_all'     => false,
				'type'         => 'plain',
				'end_size'     => 2,
				'mid_size'     => 1,
				'prev_next'    => true,
		
				'add_args'     => false,
				'add_fragment' => '',
			) );
			
			$post_displayed = $the_query1->post_count;
			$post_count = $the_query1->found_posts;


			$data['count'] = $ctr;
			$data['data'] = $item;
			$data['pagination'] = $links;
			$data['post_displayed'] = $post_displayed;
			$data['post_count'] = $post_count;
			echo json_encode($data);
			wp_reset_postdata();

			} else {
				$item['success'] = "No Result Found";
				$data['data'] = $item;
				echo json_encode($data);
		}
    }