<?php
/**
 * Template Name: About Us
 * The template for displaying about pages
 *
 * Used to display about-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these about views, you may create a
 * new template file for each one. For example, tag.php (Tag abouts),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();

?>
<div id="primary"<?php post_class(); ?>>
    <main id="main" class="site-main" role="main" style="padding-top: 100px;">

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="intro-content">
                        <h2>Who we are</h2>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="container">
                    <div class="subpage-intro">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                        the_content();
                        endwhile; else: ?>
                        <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
                    </div>
                    </div>
                    <div class="container">
                    <div class="cover_boxes" data-active-element="2">
                    <ul>
                        <?php
                            $the_query1 = new WP_Query( array('post_type' => 'aboutus') );
                                if ( $the_query1->have_posts() ) {
                                while ( $the_query1->have_posts() ) {
                                    $the_query1->the_post();
                        ?>
                        <li>
                            <div class="box">
                                <a class="thumb" href="<?php the_permalink(); ?>" target="_self">
                                    <?php the_post_thumbnail(); ?>
                                </a>
                                <div class="box_content">
                                    <h5 class="cover_box_title"><?php the_title(); ?></h5>
                                    <p><?php the_content(); ?></p>
                                   
                                </div>
                            </div>
                        </li>
                        <?php }
                        wp_reset_postdata();
                        } ?>
                    </ul>
                   

                    </div>
                    <div class="row">
                        <?php
                            $the_query1 = new WP_Query( array('post_type' => 'aboutus') );
                                if ( $the_query1->have_posts() ) {
                                while ( $the_query1->have_posts() ) {
                                    $the_query1->the_post();
                                    $ctr = 0;
                        ?>
                        <ul class="col-md-4 team-image <?php $ctr++; echo ($ctr == 0)? 'act': '';?>">
                            <?php for($x=1; $x<=5; $x++){ 
                                if (get_field('image'.$x)) {   
                            ?>
                            <li>
                                <p class="image-cont"><img src="<?php echo get_field('image'.$x); ?>" alt=""></p>
                                <p><?php echo get_field('name'.$x); ?></p>
                                <p><?php echo get_field('desc'.$x); ?></p>
                            </li>
                            <?php } }?>
                        </ul>
                        <?php }
                        wp_reset_postdata();
                        } ?>
                    </div>
                    </div>
                </div>
            </div>

    </main>
</div>

<?php get_footer() ?>